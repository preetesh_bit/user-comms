package com.bondbulls.usercomms.service;

import com.bondbulls.usercomms.dto.EmailContent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.HashMap;
import java.util.Map;

@Service
public class MailContentBuilder {

    private static final Logger logger = LogManager.getLogger(MailContentBuilder.class);

    private SimpleMailMessage templateMessage;

    @Autowired
    private TemplateEngine templateEngine;

    public String buildDashboardInvitationTemplate(String username, String messageBodyHeader, String messageBodyTail, String messageBodyUrl, String messageBodyButton, String messageBody, EmailContent emailContent) {
        logger.info("Inside the method buildDashboardInvitationTemplate");

      Map<String, Object> variables = new HashMap<String, Object>();
        variables.put("userName", username);
        variables.put("messageBodyHeader", messageBodyHeader);
        variables.put("messageBodyTail", messageBodyTail);
        variables.put("messageBodyUrl", messageBodyUrl);
        variables.put("messageBodyButton", messageBodyButton);
        variables.put("messageBody", messageBody);
        variables.put("emailContent", emailContent);
        logger.info("User Name"+username);

       return build(variables, "testTemplate.html");


    }

//    public String buildForgotPasswordTemplate(String userName, String activationLink, EmailContent emailContent) {
//        Map<String, Object> variables = new HashMap<String, Object>();
//        variables.put("userName", userName);
//        variables.put("activationLink", activationLink);
//        variables.put("emailContent", emailContent);
//        return build(variables, "forgotPassword");
//    }

    private String build(Map<String, Object> map, String templateName) {
        logger.info("Inside the method build");
        Context context = new Context();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            context.setVariable(entry.getKey(), entry.getValue());
        }
        return templateEngine.process(templateName, context);
    }
}
