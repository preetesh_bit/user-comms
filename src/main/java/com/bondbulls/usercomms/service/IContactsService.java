package com.bondbulls.usercomms.service;

import com.bondbulls.usercomms.dto.CreateContactRequestDTO;
import com.bondbulls.usercomms.dto.ResetPasswordDTO;
import com.bondbulls.usercomms.dto.UpdatePasswordRequestDTO;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

public interface IContactsService {

    boolean createContact(CreateContactRequestDTO contactRequestDTO);

    String updatePassword(UpdatePasswordRequestDTO passwordRequestDto) throws Exception;

    String resetPassword(ResetPasswordDTO resetPasswordDto) throws Exception;
    String sendResetPasswordLink(Map<String, String> requestJson) throws Exception;

    Boolean login(ResetPasswordDTO loginRequestDTO);

    Boolean confirmUser(String confirmationToken);
}
