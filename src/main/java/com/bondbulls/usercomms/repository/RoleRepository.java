package com.bondbulls.usercomms.repository;

import com.bondbulls.usercomms.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository  extends JpaRepository<Role, Long>{


        Role findByName(String role);

        @Query(value = "select * from role where id = :id",nativeQuery = true)
        Role findByRoleId(@Param("id") Long id);

}
