package com.bondbulls.usercomms.service;

import com.bondbulls.usercomms.dto.EmailContent;
import com.bondbulls.usercomms.model.Contacts;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailService {


	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private MailContentBuilder mailContentBuilder;

	@Value("${spring.mail.username}")
	private String fromEmail;


	private final Logger logger = LogManager.getLogger(MailService.class);

	public MailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}

//	public boolean sendUserInviteMail(User user, EmailContent emailContent , String messageBody1, String messageBody) throws MailException {
//		logger.info("Entered into the method sendUserInviteMail");
//		MimeMessagePreparator messagePreparator = mimeMessage -> {
//			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
//			messageHelper.setTo(user.getEmail());
//			messageHelper.setFrom(fromEmail);
//			messageHelper.setSubject(ApplicationConstants.Dashboard_Invitation);
//			String content = mailContentBuilder.buildDashboardInvitation(user.getUserName(),messageBody1, messageBody,emailContent);
//			messageHelper.setText(content, true);
//		};
//		javaMailSender.send(messagePreparator);
//		return true;
//	}

	public EmailContent getAccountContent(){
		EmailContent emailContent = null;
		try {
			logger.info("Inside the method getAccountContent");
//			emailContent = fillContentFromEnterprise(host, subDomain);
			emailContent = fillContentFromEnterprise();
		} catch(Exception e) {
			logger.error(e.getMessage(), e);
		}
		return emailContent;
	}


	private EmailContent fillContentFromEnterprise(){
		logger.info("Filling email content");
		EmailContent emailContent = new EmailContent();
		emailContent.setSupportContactName("Support");
		emailContent.setSupportContactNo("9812324354");
		emailContent.setSupportEmailId("no-reply@bondbulls.com");
		logger.info("Successfully filled the content");
		return emailContent;
	}

	public boolean sendUserInviteEMail(Contacts contact, EmailContent emailContent , String messageBodyHeader, String messageBodyTail, String messageBodyUrl, String messageBodyButton, String messageBody, String emailSubject) throws MailException {
		logger.info("Entered into the method sendUserPasswordMail");
		MimeMessagePreparator messagePreparator = mimeMessage -> {
			MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
			messageHelper.setTo(contact.getEmailId());
			messageHelper.setFrom(fromEmail);
			messageHelper.setSubject(emailSubject);
			messageHelper.setText(messageBodyUrl,true);
		//String content = mailContentBuilder.buildDashboardInvitationTemplate(contact.getName(), messageBodyHeader,messageBodyTail,messageBodyUrl, messageBodyButton, messageBody,emailContent);
			String content = messageBody;
			messageHelper.setText(content, true);
		};
		javaMailSender.send(messagePreparator);
		return true;
	}
}

