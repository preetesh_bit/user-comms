package com.bondbulls.usercomms.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "contacts")
public class Contacts {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

    public String getName() {
        return name;
    }

    @Column
    private String name;

    @Column(name="email")
    private String emailId;

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonIgnore
    @Column(name="password")
    private String password;

    @Column(name="onboarding_done")
    private boolean onboardingDone;

    public void setPasswordChanged(boolean passwordChanged) {
        this.passwordChanged = passwordChanged;
    }

    @Column(name="password_changed")
    private boolean passwordChanged;

    @Column(name = "phone")
    private String phoneNumber;

    public Boolean getEnabled() {
        return isEnabled;
    }

    @Column(name = "is_enabled")
    private Boolean isEnabled;



    @Column(name = "company_name")
    private String companyName;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "role_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Role roleId;

    @Column(name = "created_on")
    private Date createdOn;

    public Contacts() {
    }

    public Contacts(String name, String emailId, String password, boolean onboardingDone, boolean passwordChanged, String phoneNumber, Role roleId, Date createdOn,String companyName) {
        this.name = name;
        this.emailId = emailId;
        this.password = password;
        this.onboardingDone = onboardingDone;
        this.passwordChanged = passwordChanged;
        this.phoneNumber = phoneNumber;
        this.roleId = roleId;
        this.createdOn = createdOn;
        this.isEnabled = false;
        this.companyName = companyName;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }


    public Long getId() {
        return id;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getPassword() {
        return password;
    }

    public boolean isOnboardingDone() {
        return onboardingDone;
    }

    public boolean isPasswordChanged() {
        return passwordChanged;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Role getRoleId() {
        return roleId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    @Override
    public String toString() {
        return "Contacts{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", emailId='" + emailId + '\'' +
                ", password='" + password + '\'' +
                ", onboardingDone=" + onboardingDone +
                ", passwordChanged=" + passwordChanged +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", roleId=" + roleId +
                ", createdOn=" + createdOn +
                '}';
    }


}
