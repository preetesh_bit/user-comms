package com.bondbulls.usercomms.exception;

public class UserNotFoundException extends  RuntimeException{
        public UserNotFoundException(Exception e) {
            super(e);
        }

        public UserNotFoundException(String message) {
            super(message);
        }

        public UserNotFoundException(Throwable cause) {}

        public UserNotFoundException() {}
}
