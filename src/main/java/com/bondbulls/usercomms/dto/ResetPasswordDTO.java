package com.bondbulls.usercomms.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ResetPasswordDTO {

@NotBlank(message = " emailid cannot be blank")
private String emailId;

@NotBlank(message = "password cannot be blank")
private String password;


}
