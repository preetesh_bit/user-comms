package com.bondbulls.usercomms.exception;

public class MailException extends RuntimeException {

    public MailException(Exception e) {
        super(e);
    }

    public MailException(String message) {
        super(message);
    }

    public MailException(Throwable cause) {}

    public MailException() {

    }




}
