package com.bondbulls.usercomms.exception;

public class UserPasswordInvalidException extends RuntimeException{

    public UserPasswordInvalidException(Exception e) {
        super(e);
    }

    public UserPasswordInvalidException(String message) {
        super(message);
    }

    public UserPasswordInvalidException(Throwable cause) {}

    public UserPasswordInvalidException() {

    }

}
