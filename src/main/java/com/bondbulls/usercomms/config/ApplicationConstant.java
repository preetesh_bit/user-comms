package com.bondbulls.usercomms.config;

public class ApplicationConstant {



    public static final String USER_PASSWORD="Password changed successfully!!";
    public static final String DASHBOARD_PASSWORD_HEADER="Your password for Bondbulls approved successfully!";
    public static final String DASHBOARD_PASSWORD_BODY="Your password for Bondbulls  has been changed successfully";
    public static final String DASHBOARD_INVITATION_HEADER="Your Bondbulls login needs to be approved!";
    public static final String DASHBOARD_WORKSPACE_HEADER="Your Bondbulls for Environment has been approved successfully!";
    public static final String DASHBOARD_INVITATION_BUTTON="Confirm your login";
    public static final String DASHBOARD_RESET_BUTTON="Reset Password";

    public static final String USER_PASSWORDRESET_EMAIL_SUBJECT="Bondbulls account password reset!!";
    public static final String USER_PASSWORDRESET_EMAIL_BODY="Your reset password request has been approved.";

    public static final String INCORRECT_EMAIL="Incorrect email,enter the valid email";


    public static final String VERIFICATION_MAIL = "To confirm your account, please click this link : ";

    public static final String VERIFICATION_PATH = "http://alpha-application.bondbulls.com/#/login?token=";

  //  https://alpha-application.bondbulls.com/#/login?token=2a03c580-4a1c-40b8-8a76-6daa8e18bee0

    public static final String VERIFICATION_HEADER_MAIL = "Account Verification mail";

    public static final String VERIFICATION_HEADER = "Verify Account information";



}
