CREATE TABLE  role (
 id bigint(20) NOT NULL UNIQUE,
 name varchar(255) NOT NULL,
PRIMARY KEY ( id )
);

CREATE TABLE  contacts (
 id bigint(20) NOT NULL AUTO_INCREMENT,
 name varchar(255) NOT NULL,
 email varchar(255) NOT NULL,
 password_changed bit(1) NOT NULL,
 password varchar(255) NOT NULL,
 onboarding_done bit(1) NOT NULL,
 role_id bigint(20) NOT NULL,
 phone varchar(20) NOT NULL,
 created_on DATETIME NOT NULL,


 PRIMARY KEY (id),
 FOREIGN KEY ( role_id ) REFERENCES  role( id )

);