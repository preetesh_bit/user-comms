package com.bondbulls.usercomms.controller;


import com.bondbulls.usercomms.config.ResponseUtility;
import com.bondbulls.usercomms.dto.CreateContactRequestDTO;
import com.bondbulls.usercomms.dto.ResetPasswordDTO;
import com.bondbulls.usercomms.dto.UpdatePasswordRequestDTO;
import com.bondbulls.usercomms.service.IContactsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/contacts/")
public class ContactsController {

    private static final Logger logger = LogManager.getLogger(ContactsController.class);

    @Autowired
    IContactsService contactsService;


    @Autowired
    private ResponseUtility responseJsonUtil;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(4);
    }


    @RequestMapping(value = "user", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@RequestBody CreateContactRequestDTO createContactRequestDTO) {

        boolean isCreated;
        try {

            isCreated = this.contactsService.createContact(createContactRequestDTO);

        } catch (Exception e) {
            logger.error("Error while creating new user contacts " + e);
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (isCreated == true)
            return new ResponseEntity<Object>(HttpStatus.CREATED);
        else
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @RequestMapping(value = "updatePassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updatePassword(@RequestBody @Valid UpdatePasswordRequestDTO updatePasswordRequestDto) throws Exception {
        try {
            logger.info("Started updating password with Request= " + updatePasswordRequestDto);
            Map responseMap = new HashMap();
            HttpStatus status = HttpStatus.OK;
            responseMap = responseJsonUtil.getResponseJson(200, contactsService.updatePassword(updatePasswordRequestDto));
            logger.info("successfully updated Password");
            return new ResponseEntity<Object>(responseMap, HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error :ocurs while updating users password " + e);
            throw new Exception(e);
        }

    }


    @RequestMapping(value = "resetPassword", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> resetPassword(@RequestBody @Valid ResetPasswordDTO resetPasswordDto) throws Exception {
        logger.info("Started Reset Password with Request= " + resetPasswordDto);
        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;
        responseMap = responseJsonUtil.getResponseJson(200, contactsService.resetPassword(resetPasswordDto));
        logger.info("successfully sent reset-password");
        return new ResponseEntity<Object>(responseMap, HttpStatus.OK);
    }


    @RequestMapping(value = "resetLink", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> sendResetPasswordLink(@RequestBody Map<String, String> requestJson) throws Exception {
        logger.info("Started reseting password-link");
        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;
        responseMap = responseJsonUtil.getResponseJson(200, contactsService.sendResetPasswordLink(requestJson));
        logger.info("successfully sent reset-password-link");
        return new ResponseEntity<Object>(responseMap, HttpStatus.OK);
    }


    //sends 200 if valid, else 404
    @RequestMapping(value = "login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@RequestBody ResetPasswordDTO loginRequestDTO) {

        Boolean result;
        if (Objects.isNull(loginRequestDTO)) return new ResponseEntity<>((HttpStatus.BAD_REQUEST));
        try {
            result = this.contactsService.login(loginRequestDTO);
        } catch (Exception e) {

            logger.error("Error while login,Please try again", e);
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (result == true) return new ResponseEntity<Object>(HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }


    @RequestMapping(value = "/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<?> confirmUserAccount(@RequestParam("token") String confirmationToken) {
      /*  ConfirmationToken token = confirmationTokenRepository.findByConfirmationToken(confirmationToken);

        if(token != null)
        {
            Contacts conatct = (token.getUser().getEmailId());
            user.setEnabled(true);
            userRepository.save(user);
            modelAndView.setViewName("accountVerified");
        }
        else
        {
            modelAndView.addObject("message","The link is invalid or broken!");
            modelAndView.setViewName("error");
        }

        return modelAndView;*/

        Boolean isEnabled;


        isEnabled = this.contactsService.confirmUser(confirmationToken);

        if (isEnabled == true)
            return new ResponseEntity<>(HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);


    }
}