package com.bondbulls.usercomms.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


    @Entity
    @Table(name = "role")
    public class Role {

        @Getter
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="id")
        private long id;

        @Getter
        @Column(name="name")
        private String name;

        public Role(long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Role() {
        }

        public long getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

