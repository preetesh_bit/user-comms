package com.bondbulls.usercomms.controller;

import com.bondbulls.usercomms.dto.ResetPasswordDTO;
import com.bondbulls.usercomms.service.ContactsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;

import java.util.Objects;


@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/login")
public class LoginController {

@Autowired
    ContactsService contactsService;

    private static final Logger logger = LogManager.getLogger(LoginController.class);



    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> login(@RequestBody ResetPasswordDTO loginRequestDTO) {

        Boolean result;
        if (Objects.isNull(loginRequestDTO)) return new ResponseEntity<>((HttpStatus.BAD_REQUEST));
        try {
            result = this.contactsService.login(loginRequestDTO);
        } catch (Exception e) {

            logger.error("Error while login,Please try again", e);
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (result == true) return new ResponseEntity<Object>(HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }
}
