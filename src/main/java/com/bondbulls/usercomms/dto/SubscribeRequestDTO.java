package com.bondbulls.usercomms.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SubscribeRequestDTO {


    private String name;

    private String emailId;

    public SubscribeRequestDTO(String name, String emailId) {
        this.name = name;
        this.emailId = emailId;
    }
}
