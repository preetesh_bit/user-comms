package com.bondbulls.usercomms.service;

import com.bondbulls.usercomms.config.ApplicationConstant;
import com.bondbulls.usercomms.dto.CreateContactRequestDTO;
import com.bondbulls.usercomms.dto.EmailContent;
import com.bondbulls.usercomms.dto.ResetPasswordDTO;
import com.bondbulls.usercomms.dto.UpdatePasswordRequestDTO;
import com.bondbulls.usercomms.exception.InputParametersException;
import com.bondbulls.usercomms.exception.UserNotFoundException;
import com.bondbulls.usercomms.exception.UserPasswordInvalidException;
import com.bondbulls.usercomms.model.Contacts;
import com.bondbulls.usercomms.model.Role;
import com.bondbulls.usercomms.model.VerificationToken;
import com.bondbulls.usercomms.repository.ContactsRepository;
import com.bondbulls.usercomms.repository.RoleRepository;
import com.bondbulls.usercomms.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import com.bondbulls.usercomms.exception.MailException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;


@Service
public class ContactsService implements IContactsService {

    @Autowired
    ContactsRepository contactsRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired(required = true)
    PasswordEncoder passwordEncoder;

    @Autowired
    MailService mailService;

    @Autowired
    VerificationTokenRepository verificationTokenRepository;


    private static final Logger logger = LogManager.getLogger(ContactsService.class);

    @Override
    public boolean createContact(CreateContactRequestDTO contactRequestDTO) {

        boolean isCreated;
        try {

            logger.info("Validating contact information of user " + contactRequestDTO.getName() + "with email " + contactRequestDTO.getEmailId());
            Contacts contacts = this.contactsRepository.findOneByEmailId(contactRequestDTO.getEmailId());
            if (contacts != null || !Objects.isNull(contacts))
                throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User with EmailId already exists ");

            if (this.contactsRepository.findByphoneNumber(contactRequestDTO.getPhoneNumber()) != null)
                throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User with phone number already exists ");


            Role role = this.roleRepository.findByRoleId(contactRequestDTO.getRoleId());
            if (!Objects.isNull(role)) {

                Contacts contact = this.contactsRepository.saveAndFlush(new Contacts(contactRequestDTO.getName(), contactRequestDTO.getEmailId(), passwordEncoder.encode(contactRequestDTO.getPassword()), false, false, contactRequestDTO.getPhoneNumber(), role, new Date(),contactRequestDTO.getCompanyName()));
                logger.info("Saving contact details for user " + contact.getName());
                isCreated = (Objects.isNull(contact)) ? false : true;
                logger.info(" contact details saved !!");


                if (!Objects.isNull(contact)) {
                    VerificationToken token = this.verificationTokenRepository.saveAndFlush(new VerificationToken(contact));


                    String messageBodyHeader = ApplicationConstant.VERIFICATION_HEADER;
                    String messageBody = ApplicationConstant.VERIFICATION_MAIL+ApplicationConstant.VERIFICATION_PATH+token.getVerificationToken();
                    String emailSubject = ApplicationConstant.VERIFICATION_HEADER_MAIL;


                    logger.info("Inviting User through mail for password updation!!!");
                    EmailContent emailContent = mailService.getAccountContent();
                    if (mailService.sendUserInviteEMail(contact, emailContent, messageBodyHeader, "", "", "", messageBody, emailSubject)) {
                        logger.info("password updated successfully");

                    }
                }


                return isCreated;
            } else {
                throw new HttpServerErrorException(HttpStatus.BAD_REQUEST);
            }

        } catch (Exception e) {
            logger.error("Error while saving entity to database", e);

        }

        return false;


    }

    @Override
    public String updatePassword(UpdatePasswordRequestDTO userUpdatePasswordRequestDTO) throws Exception {
        logger.info("Entered into the method updatePassword and finding the user-details");
        Contacts contact = contactsRepository.findOneByEmailId(userUpdatePasswordRequestDTO.getEmailId());
        if (contact != null) {
            logger.info("Started reset-password for an user email= " + userUpdatePasswordRequestDTO.getEmailId());
            contact.setPasswordChanged(true);
            contact.setPassword(passwordEncoder.encode(userUpdatePasswordRequestDTO.getPassword()));
            contact = contactsRepository.save(contact);
            logger.info("Successfully reset the password for an user email= " + userUpdatePasswordRequestDTO.getEmailId());
            if (contact != null) {
//                    tenantAdminService.sendEmailNotification(user.getEmail(), "Password changed successfully", "Your password for Paketo Dashboard has been changed successfully");

                String messageBodyHeader = ApplicationConstant.DASHBOARD_PASSWORD_HEADER;
                String messageBody = ApplicationConstant.DASHBOARD_PASSWORD_BODY;
                String emailSubject = ApplicationConstant.USER_PASSWORD;

                logger.info("Inviting User through mail for password updation!!!");
                EmailContent emailContent = mailService.getAccountContent();
                if (mailService.sendUserInviteEMail(contact, emailContent, messageBodyHeader, "", "", "", messageBody, emailSubject)) {
                    logger.info("password updated successfully");
                    return "password updated successfully";
                } else {
                    //return "user invitation mail failure";
                    logger.error("Error : password updated failure");
                    throw new MailException();
                }

            } else {
                logger.error("Error : User Password Invalid for an Email= " + userUpdatePasswordRequestDTO.getEmailId());
                throw new UserPasswordInvalidException();
            }

        } else {
            logger.error("Error : User not found for an Email= " + userUpdatePasswordRequestDTO.getEmailId());
            throw new UserNotFoundException();
        }
    }

    @Override
    public String resetPassword(ResetPasswordDTO resetPasswordDto) throws Exception {


        logger.info("Start  resetPassword for user email: " + resetPasswordDto.getEmailId());
        Contacts userContact = contactsRepository.findOneByEmailId(resetPasswordDto.getEmailId());
        logger.info("Re-setting the password for user");
        if (!Objects.isNull(userContact)) {
            userContact.setPasswordChanged(true);
            userContact.setPassword(passwordEncoder.encode(resetPasswordDto.getPassword()));
            userContact = contactsRepository.save(userContact);
            logger.info("Successfully reset-password for an user");
            if (userContact != null) {

                String messageBodyHeader = ApplicationConstant.DASHBOARD_PASSWORD_HEADER;
                String messageBody = ApplicationConstant.DASHBOARD_PASSWORD_BODY;
                String emailSubject = ApplicationConstant.USER_PASSWORD;

                logger.info("Inviting User through mail for password updation!!!");
                EmailContent emailContent = mailService.getAccountContent();
                if (mailService.sendUserInviteEMail(userContact, emailContent, messageBodyHeader, "", "", "", messageBody, emailSubject)) {
                    logger.info("password updated successfully");
                    return "password updated successfully";
                } else {
                    //return "user invitation mail failure";
                    logger.error("Error : password mail updated failure");
                    throw new MailException();
                }
            } else {
                logger.error("Error  :User Password Invalid");
                throw new UserPasswordInvalidException();
            }

        } else {
            logger.error("Error : User not found");
            throw new UserNotFoundException("user not found");
        }
    }

    public String sendResetPasswordLink(Map<String, String> requestJson) throws Exception {
        logger.info("Entered into the method sendResetPasswordLink and started finding user for email= " + requestJson.get("email"));
        Contacts user = contactsRepository.findOneByEmailId(requestJson.get("email"));
        if (user != null) {
            //checking the user domain name
/*            String domainName=user.getTenant().getSubdomainName().substring(8);
            logger.info("domainName = "+domainName);*/
     /*       if(!domainName.equalsIgnoreCase(requestJson.get("url"))){
                logger.info("User does not have access into the tenant!!");
                throw new InputParametersException("User does not have access into the tenant!!");
            }*/
            logger.info("Started the process of reset-password link for email= " + requestJson.get("email"));
            String uuid = UUID.randomUUID().toString();
            String reseturl = "https://" + requestJson.get("url") + "/#/user/reset_password?id=" + uuid.substring(0, 4) + user.getId() + uuid.substring(4, 7);

            String messageBodyHeader = ApplicationConstant.DASHBOARD_INVITATION_HEADER;
            String messageBodyTail = "You can reset the password by clicking the below url";
            String messageBody = ApplicationConstant.USER_PASSWORDRESET_EMAIL_BODY;
            String emailSubject = ApplicationConstant.USER_PASSWORDRESET_EMAIL_SUBJECT;
            String messageBodyButton = ApplicationConstant.DASHBOARD_RESET_BUTTON;

            logger.info("Inviting User through mail for reset password link!!!");
            EmailContent emailContent = mailService.getAccountContent();
            if (mailService.sendUserInviteEMail(user, emailContent, messageBodyHeader, messageBodyTail, reseturl, messageBodyButton, messageBody, emailSubject)) {
                logger.info("User's password reset link has been sent");
                return "User's password reset link has been sent";
            } else {
                //return "user invitation mail failure";
                logger.error("Error : password mail updated failure");
                throw new MailException();
            }

//                if (!tenantAdminService.sendEmailNotification(requestJson.get("email"), ApplicationConstants.USER_PASSWORDRESET_EMAIL_SUBJECT, ApplicationConstants.USER_PASSWORDRESET_EMAIL_BODY + reseturl)) {
//                    logger.error("Error : Incorrect email");
//                    throw new InputParametersException(ApplicationConstants.INCORRECT_EMAIL);
//                }
//                logger.info("User's password reset link has been sent successfully");
//                return ApplicationConstants.USER_PASSWORDRESET_MESSAGE;
        } else {
            logger.error("Error : Incorrect email");
            throw new InputParametersException(ApplicationConstant.INCORRECT_EMAIL);
        }
    }

    @Override
    public Boolean login(ResetPasswordDTO resetPasswordDTO) {

        Contacts contact = this.contactsRepository.findOneByEmailId(resetPasswordDTO.getEmailId());

        logger.info("Trying to log you in,email: " + resetPasswordDTO.getEmailId());
        if (Objects.isNull(contact)) return false;

        if (contact.getEnabled() ) {

            logger.info("Validating user credentials");
            if (passwordEncoder.matches(resetPasswordDTO.getPassword(), contact.getPassword())) return true;
            else
                return false;

        }
        return false;
    }

@Override
    public Boolean confirmUser(String confirmationToken) {


        VerificationToken verificationToken = this.verificationTokenRepository.findByverificationToken(confirmationToken);
        if (verificationToken != null) {
            Contacts contact = this.contactsRepository.findOneByEmailId(verificationToken.getContact().getEmailId());
            contact.setEnabled(true);
            contactsRepository.saveAndFlush(contact);
            return true;
            //modelAndView.setViewName("accountVerified");
        } else {
            /*modelAndView.addObject("message", "The link is invalid or broken!");
            modelAndView.setViewName("error");*/

            return false;
        }

    }
}