package com.bondbulls.usercomms.model;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@Table(name = "verification_token")
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long tokenid;

    @Column(name="verification_token")
    private String verificationToken;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on")
    private Date createdDate;

    @OneToOne(targetEntity = Contacts.class, fetch = FetchType.EAGER)
    @JoinColumn(nullable = false, name = "contact_id")
    private Contacts contact;

    public VerificationToken(Contacts contact) {
        this.contact = contact;
        createdDate = new Date();
        verificationToken = UUID.randomUUID().toString();
    }

    public VerificationToken() {
    }
}
