CREATE TABLE  verification_token (
 id bigint(20) NOT NULL AUTO_INCREMENT,
 verification_token varchar(255) UNIQUE NOT NULL,

 created_on DATETIME NOT NULL,
 contact_id bigint(20) NOT NULL ,


 PRIMARY KEY (id),
 FOREIGN KEY ( contact_id ) REFERENCES  contacts( id )

);