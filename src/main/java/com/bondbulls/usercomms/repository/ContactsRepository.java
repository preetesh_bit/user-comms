package com.bondbulls.usercomms.repository;

import com.bondbulls.usercomms.model.Contacts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ContactsRepository extends JpaRepository<Contacts,Long> {

    Contacts findOneByEmailId(String emailId);
    Contacts findByphoneNumber(String phoneNumber);
}
