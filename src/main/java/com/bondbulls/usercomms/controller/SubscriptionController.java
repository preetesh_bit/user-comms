package com.bondbulls.usercomms.controller;


import com.bondbulls.usercomms.dto.SubscribeRequestDTO;
import com.bondbulls.usercomms.service.SubscriptionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/subscribe/")
public class SubscriptionController {


    @Autowired
    SubscriptionService subscriptionService;

    private static final Logger logger = LogManager.getLogger(SubscriptionController.class);

    public void subscribe(@RequestBody SubscribeRequestDTO subscribeRequestDTO, HttpServletResponse response) {

        if (Objects.isNull(subscribeRequestDTO))
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST, "User email or name not found");

        try {
            this.subscriptionService.subscribeService(subscribeRequestDTO);
            response.setStatus(200);

        } catch (Exception e) {

            logger.info(e);
            response.setStatus(500);

        }


    }
}