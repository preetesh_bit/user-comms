package com.bondbulls.usercomms.exception;

public class InputParametersException extends RuntimeException{

    public InputParametersException(Exception e) {
        super(e);
    }

    public InputParametersException(String message) {
        super(message);
    }

    public InputParametersException(Throwable cause) {}

    public InputParametersException() {

    }
}
