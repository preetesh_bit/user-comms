package com.bondbulls.usercomms.dto;


import lombok.Data;

@Data
public class EmailContent {


    private String supportContactName;
    private String supportContactNo;
    private String supportEmailId;
    
}
