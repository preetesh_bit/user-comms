package com.bondbulls.usercomms.config;


import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class ResponseUtility {

    public Map<?, ?> getResponseJson(int status, Map<String, Object> respJson) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", respJson);
        return responseBuilder;
    }

    public Map<?, ?> getResponseJson(int status, String respJson) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", respJson);
        return responseBuilder;
    }


    public Map<String, Object> getResponseJson(int status, Set respJson) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", respJson);
        return responseBuilder;
    }

    public Map<String, Object> getResponseJson(int status, Object a) {
        Map<String, Object> responseBuilder = new HashMap<>();
        responseBuilder.put("status", status);
        responseBuilder.put("data", a);
        return responseBuilder;
    }
}
