package com.bondbulls.usercomms.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UpdatePasswordRequestDTO {


    @Email(message = "email should be valid")
    @NotBlank(message = "email cannot be blank")
    private String emailId;

    @NotBlank(message = "password cannot be blank")
    private String password;
}
