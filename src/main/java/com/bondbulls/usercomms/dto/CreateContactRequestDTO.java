package com.bondbulls.usercomms.dto;

import com.bondbulls.usercomms.model.Role;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
public class CreateContactRequestDTO {


    private String name;
    private String emailId;
    private String password;
    private String phoneNumber;
    private Long roleId;
    private String companyName;


    public CreateContactRequestDTO() {
    }
}




