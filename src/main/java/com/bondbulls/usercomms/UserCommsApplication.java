package com.bondbulls.usercomms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication()
public class UserCommsApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserCommsApplication.class, args);
	}

}
